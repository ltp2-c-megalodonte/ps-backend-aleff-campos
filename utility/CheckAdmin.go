package utility

import (
	database "projetov2/Backend-projeto/Database"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

func CheckAdm(filter primitive.D) (bool, error) {

	resultado, err := database.FindOneUser(filter)

	if err != nil {
		return false, err
	}

	if !resultado.Admin {
		return false, nil
	}
	return true, nil

}
