package utility

import (
	"net/http"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

const JWT_SECRET_KEY = "mykey"

// func GenerateToken(username string) (string, error) {
// 	expirationTime := time.Now().Add(time.Minute * 5)

// 	claims := jwt.MapClaims{
// 		"username": username,
// 		"expires":  expirationTime.Unix(),
// 	}
// 	//gera token
// 	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
// 	//assina token
// 	return token.SignedString([]byte(JWT_SECRET_KEY))

// }

func GenerateToken(username string, role string) (string, error) {
	expirationTime := time.Now().Add(time.Minute * 5)

	claims := jwt.MapClaims{
		"username": username,
		"role":     role,
		"expires":  expirationTime.Unix(),
	}

	// Gera o token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Assina o token
	return token.SignedString([]byte(JWT_SECRET_KEY))
}

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Extrai o token do cookie
		cookie, err := r.Cookie("token")
		if err != nil {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		tokenStr := cookie.Value
		claims := &jwt.MapClaims{}

		// Verifica e analisa o token
		token, err := jwt.ParseWithClaims(tokenStr, claims, func(token *jwt.Token) (interface{}, error) {
			return []byte(JWT_SECRET_KEY), nil
		})

		if err != nil || !token.Valid {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		// Checa o papel (role) do usuário
		role, ok := (*claims)["role"].(string)
		if !ok {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		// Lógica de acesso baseada em papel e rota
		if r.URL.Path == "/admin" && role != "admin" {
			http.Error(w, "Forbidden: Admins Only", http.StatusForbidden)
			return
		}

		// Permite que a requisição prossiga
		next.ServeHTTP(w, r)
	})
}
